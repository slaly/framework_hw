<?php

require_once('vendor/autoload.php');

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;  //เป็นการสร้าง entity manager 

$isDevMode = false;

$dbParam = array (
'driver' => 'sqlsrv',
'user' => 'seals',
'password' => 'seals',
'dbname' => 'seals',
'host' => 'localhost\SQLEXPRESS',
'charset' => 'utf-8',
);

$paths = array(__DIR__ . "/entities");

$config = Setup::createAnnotationMetadataConfiguration($paths,$isDevMode, null, null, false);
$entityManager = EntityManager::create($dbParam,$config);
if($entityManager){
    echo "sucsess";
}else{
    echo "connection fail";
}
