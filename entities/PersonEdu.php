<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * PersonEdu
 *
 * @ORM\Table(name="person_edu", indexes={@ORM\Index(name="IX_person_edu", columns={"person_id"})})
 * @ORM\Entity
 */
class PersonEdu
{
    /**
     * @var int
     *
     * @ORM\Column(name="person_edu_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $personEduId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="edu_desc", type="string", length=50, nullable=true)
     */
    private $eduDesc;

    /**
     * @var \Person
     *
     * @ORM\ManyToOne(targetEntity="Person")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="person_id", referencedColumnName="person_id")
     * })
     */
    private $person;



    /**
     * Get personEduId.
     *
     * @return int
     */
    public function getPersonEduId()
    {
        return $this->personEduId;
    }

    /**
     * Set eduDesc.
     *
     * @param string|null $eduDesc
     *
     * @return PersonEdu
     */
    public function setEduDesc($eduDesc = null)
    {
        $this->eduDesc = $eduDesc;

        return $this;
    }

    /**
     * Get eduDesc.
     *
     * @return string|null
     */
    public function getEduDesc()
    {
        return $this->eduDesc;
    }

    /**
     * Set person.
     *
     * @param \Person|null $person
     *
     * @return PersonEdu
     */
    public function setPerson(\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person.
     *
     * @return \Person|null
     */
    public function getPerson()
    {
        return $this->person;
    }
}
