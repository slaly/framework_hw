<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * PersonAddress
 *
 * @ORM\Table(name="person_address", indexes={@ORM\Index(name="IDX_2FD0DC08217BBB47", columns={"person_id"})})
 * @ORM\Entity
 */
class PersonAddress
{
    /**
     * @var string
     *
     * @ORM\Column(name="addr_type", type="string", length=5, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $addrType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="addr", type="text", length=16, nullable=true)
     */
    private $addr;

    /**
     * @var \Person
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Person")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="person_id", referencedColumnName="person_id")
     * })
     */
    private $person;



    /**
     * Set addrType.
     *
     * @param string $addrType
     *
     * @return PersonAddress
     */
    public function setAddrType($addrType)
    {
        $this->addrType = $addrType;

        return $this;
    }

    /**
     * Get addrType.
     *
     * @return string
     */
    public function getAddrType()
    {
        return $this->addrType;
    }

    /**
     * Set addr.
     *
     * @param string|null $addr
     *
     * @return PersonAddress
     */
    public function setAddr($addr = null)
    {
        $this->addr = $addr;

        return $this;
    }

    /**
     * Get addr.
     *
     * @return string|null
     */
    public function getAddr()
    {
        return $this->addr;
    }

    /**
     * Set person.
     *
     * @param \Person $person
     *
     * @return PersonAddress
     */
    public function setPerson(\Person $person)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person.
     *
     * @return \Person
     */
    public function getPerson()
    {
        return $this->person;
    }
}
