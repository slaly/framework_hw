var gulp = require('gulp');

var minifyCSS = require('gulp-minify-css');
var minifyJS = require('gulp-uglify');
var rename = require('gulp-rename');
var sass = require('gulp-ruby-sass');

var libJS = [
    
    './Resources/assets/lib/bootstrap/dist/js/bootstrap.min.js',
    './Resources/assets/lib/jquery/dist/jquery.min.js',
    './Resources/assets/lib/jquery-validation/dist/jquery.validate.min.js',
    './Resources/assets/lib/datatables/media/js/jquery.dataTables.js',
    './Resources/assets/lib/datatables/media/js/dataTables.bootstrap4.min.js',
    './Resources/assets/lib/jquery-validation/dist/additional-methods.min.js'
];

var libCSS = [
    
    './Resources/assets/lib/bootstrap/dist/css/bootstrap.min.css',
   './Resources/assets/lib/datatables/media/css/dataTables.bootstrap4.min.css',
   './Resources/assets/lib/datatables/media/css/jquery.dataTables.min.css',
];




//ย่อไฟล์

gulp.task ('libjs',function(){
    return gulp
    .src(libJS)    
    .pipe(gulp.dest('./public/js'));
});

gulp.task ('libcss',function(){
    return gulp
    .src(libCSS)    
    .pipe(gulp.dest('./public/css'));
});



