<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="css\bootstrap.min.css">
    <link rel="stylesheet" href="css\dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="css\jquery.dataTables.min.css">
</head>
<body>
    @yield('content')

    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.validate.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.bootstrap4.min.js"></script>
    <script src="js/additional-methods.min.js"></script>
    <script src="js/bootstrap.min.js"></script>


    @yield('script')
    
<!--<button onClick = "getData()"> Get Data</button>-->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
crossorigin="anonymous"> </script>
<script <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
</body>
</html>