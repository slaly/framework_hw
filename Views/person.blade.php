@extends('layout')

@section('content')
   <h1><center>PERSON PAGE</center></h1>

   
   <table id ="tbPerson" class="table table-striped table-bordered" >
        <thead >
            <tr>
                <td>Person ID</td>
                <td>Position ID</td>
                <td>PreName</td>
                <td>FirstName</td>
                <td>LastName</td>
                <button  class="btn btn-outline-primary" id = "add"onclick="location.href = 'http://localhost/myframework/addperson';">ADD</button>
                <button  class="btn btn-outline-success" id ="edit" onclick="location.href = 'http://localhost/myframework/editperson';">EDIT</button>
                <button  class="btn btn-outline-danger" id = "delect">DELETE</button>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@endsection

@section('script')

<script>
    $(document).ready(function () {
        $('#tbPerson').DataTable({
            
            'ajax' : 'person',
            columns:[
                {'data' : 'person_id'},
                {'data' : 'position_id'},
                {'data' : 'prename'},
                {'data' : 'fname'},
                {'data' : 'lanme'}
            ],
            responsive: true,
            fixedHeader: true
        });
       
    });

</script>
@endsection